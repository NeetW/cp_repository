var tabGroup = Titanium.UI.createTabGroup();
	
var windowHome = Titanium.UI.createWindow({
	title:"Home",
	backgroundColor:"white",
	//url:"HomeWindow.js"
});

var tabHome = Titanium.UI.createTab({
	title:"Home",
	window:windowHome	
});

var lableSLIIT1 = Ti.UI.createLabel({
	text:"Sri Lanka Institute of Information Technology",
	color:"orange",
	font:{fontSize:20},
	height:Ti.UI.SIZE,
	width:Ti.UI.SIZE,
	top:10,
	//left : myLeft,
	textAlign:'center'
});
windowHome.add(lableSLIIT1);

var windowProfile = Titanium.UI.createWindow({
	title:"Profile",
	backgroundColor:"white",
	tabBarHidden:true,
	url:"ProfileWindow.js"
});

var tabProfile = Titanium.UI.createTab({
	title:"Profile",
	touchEnabled:false,
	window:windowProfile,
	backgroundDisabledColor:'gray'
});

var windowCourse = Titanium.UI.createWindow({
	title:"Course",
	backgroundColor:"white",
	url:"CourseWindow.js"
});

var tabCourse = Titanium.UI.createTab({
	title:"Course",
	//active:false,
	window:windowCourse
});

tabGroup.addTab(tabHome);
tabGroup.addTab(tabCourse);
tabGroup.addTab(tabProfile);
//tabProfile.setVisible(false);
tabGroup.open();

/*Login Function START*/

//creating a view for the login
var viewLogin = Titanium.UI.createView({
	height:200,
	width:"auto",
	top:100,
	backgroundColor:"white"
});		
windowHome.add(viewLogin);	//adding viewLogin to windowHome

//creating a lable to indicate username	
var lableUserName = Ti.UI.createLabel({
	text:"User Name",
	color:'black',
	font:{fontSize:20},
	height:Ti.UI.SIZE,
	width:Ti.UI.SIZE,
	top:10,
	left:10,
	textAlign:'left'
});		
viewLogin.add(lableUserName);	//adding the lableUserName to viewLogin

//creating a textfield to enter username
var textfieldUserName = Titanium.UI.createTextField({
	top:10,
	heigh:"10%",	
	left:120,
	width:"50%",	
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	hintText:"IT Number",
	keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
    returnKeyType:Titanium.UI.RETURNKEY_DEFAULT
});		
viewLogin.add(textfieldUserName);	//adding the textfieldUserName to viewLogin

//creating a lable to indicate password
var lablePassword = Titanium.UI.createLabel({
	text:"Password",
	color:'black',
	font:{fontSize:20},
	height:Ti.UI.SIZE,
	width:Ti.UI.SIZE,
	top:60,
	left:10,
	textAlign:'left'
});		
viewLogin.add(lablePassword);	//adding the lablePassword to viewLogin

//creating a textfield to enter the password
var textfieldPassword = Titanium.UI.createTextField({
	top:60,
	heigh:"10%",
	left:120,
	width:"50%",
	borderStyle:Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	hintText:"Password",
	passwordMask:true,
	keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
    returnKeyType:Titanium.UI.RETURNKEY_DEFAULT
});
viewLogin.add(textfieldPassword);	//adding the textfieldPassword to viewLogin

//creating a button for Login
var buttonLogin = Titanium.UI.createButton({
	title:"Login",
	width:"25%",
	height:"20%",
	top:110,
	left:10
});		
viewLogin.add(buttonLogin);	//adding buttonLogin to viewLogin 

//creating a button for logout
var buttonLogout = Titanium.UI.createButton({
	title:"Logout",
	width:"15%",
	height:"5%",
	top:40,
	right:20
});

//creating a lable to indicate Logged in User
var labelUser = Titanium.UI.createLabel({
	text:'Welcome ',
	color:'blue',
	top:40,
    left:10,
    width:300,
    height:'auto'
});		

buttonLogout.addEventListener('click',function(e){
	
	tabHome.open();
	
	viewLogin.setVisible(true);
	
	buttonLogout.setVisible(false);
	
	labelUser.setVisible(false);
	
	tabGroup.removeTab(tabProfile);
	windowProfile.close();
	windowProfile.tabBarHidden = true;
	tabGroup.open();
});

var loginReq = Titanium.Network.createHTTPClient();
var sId;
//data handling for when PHP returns something
loginReq.onload = function()
{
    var json = this.responseText;
    var response = JSON.parse(json);
    if (response.logged == true)
    {
        textfieldUserName.blur();
        textfieldPassword.blur(); 
    
    	textfieldUserName.value = '';
        textfieldPassword.value = '';
    	
    	alert("You have successfully logged in");
        
    	viewLogin.setVisible(false);
    	
    	//tabGroup.addTab(tabProfile);
    	//windowProfile.tabBarHidden = false;
    	//tabGroup.open();
    	//tabHome.open();
    	
    	windowHome.add(buttonLogout);
    	buttonLogout.setVisible(true);
    	
    	windowHome.add(labelUser);
    	labelUser.setVisible(true);
    }
    else
    {
        alert(response.message);	
    }
};
    
buttonLogin.addEventListener('click',function(e)
{
    if (textfieldUserName.value != '' && textfieldPassword.value != '')
    {
        loginReq.open("POST","http://10.0.2.2/post_auth.php");
        var params = {
            username: textfieldUserName.value,
            password: Ti.Utils.md5HexDigest(textfieldPassword.value)
        };
        loginReq.send(params);
    }
    else
    {
        alert("Username/Password are required");
    }
});
/*Login Function END*/



